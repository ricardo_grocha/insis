﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelMicroService.Model
{
    public class ChannelModel
    {
        public int id { get; set; }
        public List<ScheduleModel> Scheduling { get; set; }
    }

    public class ScheduleModel
    {
        public int id { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
    }

    public class ChannelSimpleModel
    {
        public int id { get; set; }
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
    }

}
