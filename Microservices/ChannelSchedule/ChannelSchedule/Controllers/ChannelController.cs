﻿using System;
using System.Linq;
using ChannelMicroService.Model;
using LiteDB;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;

namespace ChannelMicroService.Controllers
{
    [Produces("application/json")]
    [Route("api/Channel")]

    public class ChannelController : Controller
    {
        private IMongoDatabase GetMongoDatabase()
        {
            var conn = $"mongodb://insis:insis@cluster0-4vg73.mongodb.net/test?retryWrites=true";
            //var conn = $"mongodb://localhost:27020";
            var client = new MongoClient(conn);
            var database = client.GetDatabase("test");

            return database;
        }

        // GET api/values
        /// <summary>
        ///  Get all channels informations
        /// </summary>
        /// <returns>all channel scheduling information</returns>
        [HttpGet]
        public ActionResult Get()
        {
            // Open database (or create if not exits)
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                
                var schedule = db.GetCollection<ScheduleModel>("Channel");
                var list = schedule.FindAll();
                //var results = customers.Find(x => x.Name.StartsWith("Jo"));

                return Json(list);
            }

            var database = GetMongoDatabase();

            var channels = database.GetCollection<ChannelModel>("Channel");

            return Json(channels);
        }

        /// <summary>
        /// Get channel information based on channel id
        /// </summary>
        /// <param name="channelId">channel ID</param>
        /// <returns>channel scheduling information</returns>
        [HttpGet("{channelId}")]
        public ActionResult Get(int channelId)
        {
            // Open database (or create if not exits)
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                
                var schedule = db.GetCollection<ScheduleModel>("Channel");
                var list = schedule.Find(c => c.id == channelId);


                return Json(list);
            }

            var database = GetMongoDatabase();

            var channel = database.GetCollection<ChannelModel>("Channel").Find(c => c.id == channelId);

            return Json(channel);
        }

        // POST api/values
        /// <summary>
        /// Create alocation of channel to some date
        /// </summary>
        /// <param name="model">Channel information</param>
        /// <returns>Json with status and error message </returns>
        [HttpPost]
        public ActionResult Post([FromBody]ChannelSimpleModel model)
        {
            try
            {
                // Open database (or create if not exits)
                using(var db = new LiteDatabase(@"MyData.db"))
                {
                    var startDate = DateTime.Parse(model.StartDateTime);
                    var endDate = DateTime.Parse(model.StartDateTime);
                    // Get customer collection
                    var schedule = db.GetCollection<ScheduleModel>("Channel");

                    // Create your new customer instance
                    var newEvent =   new ScheduleModel
                    {
                        id = model.id,
                        StartDateTime = startDate,
                        EndDateTime = endDate
                    };

                    // Insert new customer document (Id will be auto-incremented)
                    var newValue = schedule.Insert(newEvent);
                    return Json(new { Status = true, Error = "" });
                    //// Update a document inside a collection
                    //customer.Name = "Joana Doe";

                    //customers.Update(customer);

                    //// Index document using a document property
                    //customers.EnsureIndex(x => x.Name);

                    //// Use Linq to query documents
                    //var results = customers.Find(x => x.Name.StartsWith("Jo"));
                }

            }
            catch (Exception e)
            {
                return Json(new
                {
                    Status = false,
                    Error = e.Message
                });
            }


            //try
            //{
            //    var database = GetMongoDatabase();

            //    var startDate = DateTime.Parse(model.StartDateTime);
            //    var endDate = DateTime.Parse(model.StartDateTime);
            //    var schedule = database.GetCollection<ScheduleModel>("Channel")
            //        .Find(c => c.id == model.id && c.StartDateTime <= startDate && c.EndDateTime >= startDate)
            //        .ToList();
            //    if (schedule.Count() == 0)
            //    {
            //        return Json(new { Status = false, Error = "Bloco já se encontra preenchido." });
            //    }

            //    database.GetCollection<ScheduleModel>("Channel").InsertOne(
            //           new ScheduleModel
            //           {
            //               id = model.id,
            //               StartDateTime = startDate,
            //               EndDateTime = endDate
            //           });

            //    return Json(new { Status = true, Error = "" });


            //}
            //catch (Exception e)
            //{
            //    return Json(new
            //    {
            //        Status = false,
            //        Error = e.Message
            //    });
            //}

        }
    }
}