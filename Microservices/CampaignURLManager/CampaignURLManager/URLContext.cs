﻿using LiteDB;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace CampaignURLManager
{
    public class URLContext {
        //private IMongoDatabase _database { get; set; }
        private LiteDatabase _database { get; set; }

        //private IMongoCollection<URL> _URLs => _database.GetCollection<URL>("urls");
        private LiteCollection<URL> _URLs => _database.GetCollection<URL>("urls");

        public URLContext()
        {
            _database = new LiteDatabase(@"MyData.db");
            
            //var cliente = new MongoClient("mongodb+srv://admin:admin@insis-lhi3u.mongodb.net/test?retryWrites=true");
            //_database = cliente.GetDatabase("URLDB");
        }

        public URL Insert(URL url)
        {
            url.Id = LiteDB.ObjectId.NewObjectId().ToString();
            url.Link = url.LinkTemplate.Replace("{ref}", url.Id);
            _URLs.Insert(url);
            return url;
        }

        public URL GetURL(string id)
        {
            try
            {
                return _URLs.FindOne(_ => _.Id == id);
            }
            catch
            {
                return null;
            }
            
        }

        public void SetLinkHasAccessed(string id)
        {
            var oldValue = _URLs.FindOne(f => f.Id == id);
            oldValue.HadAccess = true;
            _URLs.Update(oldValue);
            //_URLs.FindOneAndUpdate<URL>(
            //    Builders<URL>.Filter.Eq("Id", id),
            //    Builders<URL>.Update.Set("HasAccess", true)
            //);
        }
    }
}
