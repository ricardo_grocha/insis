﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace CampaignURLManager.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class URLManagerController : Controller
    {
        private readonly IConfiguration configuration;
        private readonly URLContext URLContext = new URLContext();

        public URLManagerController(IConfiguration config)
        {
            configuration = config;
        }

        // GET api/URLManager/{URLID}
        /// <summary>
        /// Get an URL base on link-template created on POST
        /// </summary>
        /// <param name="URLID">URR ID</param>
        /// <returns>return an url string</returns> 
        [HttpGet("{URLID}")]
        public ActionResult Get(string URLID)
        {
            URL url = URLContext.GetURL(URLID);
            if(url is null)
                return NotFound("There is no URL with the URLID specified");
            else
                return Ok(url);
        }

        // POST api/URLManager
        /// <summary>
        /// Creates an new URL
        /// </summary>
        /// <param name="url">URL necessary data</param>
        /// <returns>return ok if everythings goes well</returns>
        [HttpPost]
        //public void Post([FromBody]int CampaignID, [FromBody]string ChannelID, [FromBody]string RecipientEmail, [FromBody]string LinkTemplate)
        public ActionResult Post([FromBody]URL url)
        {
            return Ok(URLContext.Insert(url));
        }

        // PUT api/URLManager/{URLID}/HadAccess
        /// <summary>
        /// Updates the state of the url as access to true. No need to pass any payload information.
        /// </summary>
        /// <param name="URLID">URL ID</param>
        /// <returns>return ok if everything goes well</returns>
        [HttpPut("{URLID}/Access/")]
        public IActionResult Put(string URLID)
        {
            URLContext.SetLinkHasAccessed(URLID);
            return Ok("Sucess!");
        }
    }
}
