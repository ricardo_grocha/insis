﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CampaignURLManager
{
    public class URL
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public int CampaignID { get; set; }
        public string ChannelID { get; set; }
        public string RecipientEmail { get; set; }
        public bool HadAccess { get; set; }
        public string LinkTemplate {get;set;}
        public string Link { get; set; }
        public URL() { }

        public URL(int CampaignID, string ChannelID, string RecipientEmail, string LinkTemplate)
        {
            this.Id = ObjectId.GenerateNewId().ToString();
            this.CampaignID = CampaignID;
            this.ChannelID = ChannelID;
            this.RecipientEmail = RecipientEmail;
            this.HadAccess = HadAccess;
            this.LinkTemplate = LinkTemplate;
            this.Link = this.LinkTemplate.Replace("{Id}", Id);
        }


        public static bool operator !=(URL x, URL y)
        {
            if (x is null) return (y is null);

            return !(x.Id == y.Id && x.RecipientEmail == y.RecipientEmail
                && x.HadAccess == y.HadAccess && x.LinkTemplate == y.LinkTemplate);
        }

        public static bool operator ==(URL x, URL y)
        {
            if (x is null) return (y is null);

            return (x.Id == y.Id && x.RecipientEmail == y.RecipientEmail
                && x.HadAccess == y.HadAccess && x.LinkTemplate == y.LinkTemplate);
        }
    }
}
