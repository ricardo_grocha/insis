using CampaignURLManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;

namespace Tests
{
    [TestClass]
    public class UTDataBase
    {
        private readonly URLContext URLContext = new URLContext();

        [TestMethod]
        public void CreateURLEntry()
        {
            URL url = new URL(1, "gmail", "recipientEmail@isep.pt", "https://www.link.template.pt/");
            URLContext.Insert(url);
            URL databaseURL = URLContext.GetURL(url.Id);
            Assert.IsTrue(url == databaseURL);
        }

        [TestMethod]
        public void SetLinkHasAccessed()
        {
            URL url = new URL(1, "gmail", "recipientEmail@isep.pt", "https://www.link.template.pt/{Id}");
            URLContext.Insert(url);
            URLContext.SetLinkHasAccessed(url.Id);
            URL newURL = URLContext.GetURL(url.Id);
            Assert.IsTrue(newURL.HadAccess);
        }

        //[TestMethod]
        //public void UpdateURLEntry()
        ////{
        ////    URL url = new URL("recipientEmail@isep.pt", false, "https://www.link.template.pt/");
        ////    URL urlToUpdate = new URL("recipientEmail@isep.pt2", true, "https://www.link.temp2late.pt/");
        ////    URLContext.URLs.InsertOne(url);
        ////    URL databaseURL = URLContext.URLs.FindOneAndUpdate(
        ////            _ => _.Id == url.Id, 
        ////            new UpdateDefinition<URL>(urlToUpdate),
        ////            new UpdateOptions()
        ////            {
        ////                IsUpsert = false
        ////            }
        ////        );
        ////    databaseURL.HasAccess = true;
        ////    URLContext.URLs.UpdateOne(databaseURL, Up)

        ////    Assert.IsTrue(url == databaseURL);
        //}
    }
}
