﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using DigitalAdsDatabase;
using DigitalAdsDb;


namespace DestinyApi.Controllers
{
    public class DestinyController : ApiController
    {
        /// <summary>
        /// Select de informacoes do destinatario
        /// </summary>
        /// <param name="item">destinatario</param>
        /// <returns></returns>
        private static object SelectDestiny(DigitalAdsDb.Destiny item)
        {
            return new
            {
                item.DestinyId,
                item.Name,
                item.Email,
                item.Coin,
                item.Country,
                item.Age
            };
        }

        /// <summary>
        /// Get todos os destinatarios existentes na BD
        /// </summary>
        /// <returns>Lista de destinatarios</returns>
        // GET: api/<controller>
        [HttpGet]
        [Route("destinatario")]
        public IHttpActionResult Get()
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.Destiny.ToList().Select(SelectDestiny);

                return Ok(item);
            }
        }

        /// <summary>
        /// Get de destinatario especifico
        /// </summary>
        /// <param name="id">id do destinatario</param>
        /// <returns>Informacao do destinatario</returns>
        [HttpGet]
        [Route("destinatario/{id}")]
        public IHttpActionResult Get(int id)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.Destiny
                    .Where(x => x.DestinyId == id)
                    .ToList().Select(SelectDestiny);

                return Ok(item);
            }
        }

        /// <summary>
        /// Criacao de novo destinatario
        /// </summary>
        /// <param name="item">informacao do destinatario</param>
        /// <returns>Resultado da criacao</returns>
        // POST api/<controller>
        [Route("destinatario")]
        public IHttpActionResult Post([FromBody]Destiny item)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                if (item != null)
                {
                    ctx.Destiny.Add(item);
                }
                ctx.SaveChanges();
                return Ok(item);
            }
        }

        /// <summary>
        /// Alteracao de destinatario
        /// </summary>
        /// <param name="id">id do destinatario</param>
        /// <param name="newItem">informacao do destinatario</param>
        /// <returns>destinatario actualizado</returns>
        // PUT api/<controller>/5
        [HttpPut]
        [Route("destinatario/{id}")]
        public IHttpActionResult Put(int id, [FromBody]Destiny newItem)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.Destiny
                    .Single(x => x.DestinyId == id);

                if (newItem != null && item != null)
                {
                    item.Name = newItem.Name;
                }
                ctx.SaveChanges();
                return Ok(item);
            }
        }

        /// <summary>
        /// Delete de destinatario
        /// </summary>
        /// <param name="id">id do destinatario</param>
        // DELETE api/<controller>/5
        [HttpDelete]
        [Route("destinatario/{id}")]
        public void Delete(int id)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.Destiny
                    .Single(x => x.DestinyId == id);

                ctx.Destiny.Remove(item);
                ctx.SaveChanges();
            }
        }

        [HttpGet]
        [Route("destinatario/Generate/{nr}")]
        public IHttpActionResult GenerateEntities(int nr)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var countryArray = new List<string>{"PT", "ES", "FR", "CH"};

                var lastDest = ctx.Destiny
                    .OrderByDescending(x => x.DestinyId)
                    .FirstOrDefault();
                var startId = 1;
                if (lastDest != null)
                    startId = lastDest.DestinyId+1;

                for (var i = startId; i < (startId + nr); i++)
                {
                    int age = DatabaseHelper.GenerateNumber(16,89);
                    var country = countryArray[DatabaseHelper.GenerateNumber(0, 4)];

                    var dest = new Destiny
                    {
                        Name = "Destinatario"+ i,
                        Age = age,
                        Country = country,
                        Email = "mail2018spam@gmail.com",
                        Coin = "EUR",
                        
                        
                    };

                    ctx.Destiny.Add(dest);
                }
                var nrChanges = ctx.SaveChanges();
                return Ok(nrChanges);
            }
        }
    }
}
