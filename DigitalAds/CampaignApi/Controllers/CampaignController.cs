﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.DynamicData;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.UI;
using CampaignApi.Models;
using DigitalAdsDatabase;
using DigitalAdsDb;
using Newtonsoft.Json;

namespace Campaign.Controllers
{
    public class CampaignController : ApiController
    {
        /// <summary>
        /// Select aplicado as campanhas
        /// </summary>
        /// <param name="item">item de campanha</param>
        /// <returns></returns>
        private static object SelectCampaignReport(DigitalAdsDb.Campaign item)
        {
            return new
            {
                item.CampaignId,
                Goal = item.Goal ?? "",
                Name = item.Name ?? "",
                StateId = item.State?.StateId,
                StateName = item.State?.Name,
                Comment = item.ReviewComment,
                Channels = item.CampaignChannel.Select(SelectCampaignChannelString).StringAggregation((y, z) => $"[Canal:{y},\n\n{z}]", String.Empty),
                Coin = item.CampaignProfile?.Coin ?? "",
                Country = item.CampaignProfile?.Country ?? "",
                MaxAge = item.CampaignProfile?.maxAge ?? 0,
                MinAge = item.CampaignProfile?.minAge ?? 0,
                StartDate = item.CampaignProfile?.StartDate.ToString() ?? "",
                ElapsedTime = item.CampaignProfile?.ElapsedTime.ToString() ?? "",

            };
        }

        private static object SelectCampaign(DigitalAdsDb.Campaign item)
        {
            return new
            {
                item.CampaignId,
                Goal = item.Goal ?? "",
                Name = item.Name ?? "",
                StateId = item.State?.StateId,
                StateName = item.State?.Name,
                Comment = item.ReviewComment,
                Channels = item.CampaignChannel.Select(SelectCampaignChannel).ToList(),
                Coin = item.CampaignProfile?.Coin ?? "",
                Country = item.CampaignProfile?.Country ?? "",
                MaxAge = item.CampaignProfile?.maxAge ?? 0,
                MinAge = item.CampaignProfile?.minAge ?? 0,
                StartDate = item.CampaignProfile?.StartDate.ToString() ?? "",
                ElapsedTime = item.CampaignProfile?.ElapsedTime.ToString() ?? "",
                NumberDestaniesSent = item.NumberDestaniesSent ?? 0,
                NumberDestaniesFailed = item.NumberDestaniesFailed ?? 0

            };
        }


        /// <summary>
        /// Select aplicado aos canais
        /// </summary>
        /// <param name="item">item de canal</param>
        /// <returns></returns>
        private static object SelectCampaignChannel(DigitalAdsDb.CampaignChannel item)
        {
            return new
            {
                item.IdCampaign,
                item.IdChannel,
                Name = item.Channel?.Name ?? "",
                ChannelContent = item.ChannelContent?.Body ?? "",
                LinkTemplate = item.ChannelContent?.LinkTemplate ?? "",
            //    Coin = item.ChannelReceiverProp?.Coin ?? "",
            //    Country = item.ChannelReceiverProp?.Country ?? "",
            //    MaxAge = item.ChannelReceiverProp?.maxAge ?? 0,
            //    MinAge = item.ChannelReceiverProp?.minAge ?? 0,
            //    StartDate = item.StartDate.ToString() ?? "",
            //    EndDate = item.EndDate.ToString() ?? "",
            //    ElapsedTime = (item.EndDate - item.StartDate).GetValueOrDefault().TotalMinutes,
            //    StartDateServerHour = item.StartDate.GetValueOrDefault().AddHours(GetTimeZone(item.ChannelReceiverProp?.Country))
            };
        }

        private static string SelectCampaignChannelString(DigitalAdsDb.CampaignChannel item)
        {
            if (item == null)
            {
                return "Canal: --";
            }

            var canalInfo = $"{item.Channel.Name}\n";
            canalInfo += (item.ChannelContent == null) ? $" - Conteudo : --" : $" - Conteudo: {item.ChannelContent?.Body}\n";
            //canalInfo += (item.ChannelReceiverProp == null) ? $" - Conteudo : --" : $" - Conteudo: {item.ChannelContent?.Body}\n";
            //canalInfo += (item.ChannelReceiverProp == null) ? $" - Ages : --\n -Moeda: --\n -Pais:--" : $" -Ages:{item.ChannelReceiverProp?.minAge}-{item.ChannelReceiverProp?.maxAge}\n  -Moeda:{item.ChannelReceiverProp?.Coin}\n  -Pais:{item.ChannelReceiverProp?.Country}";

            return canalInfo;
        }

        /// <summary>
        /// Select de tabela Channel Receiver Prop
        /// </summary>
        /// <param name="item">item de ChannelReceiverProp</param>
        /// <returns></returns>
        private static object SelectChannelReceiverProp(DigitalAdsDb.CampaignProfile item)
        {
            return new
            {
                item?.Coin,
                item?.Country,
                item?.maxAge,
                item?.minAge,
                StartDate = item.StartDate.ToString(),
                item.ElapsedTime
            };
        }
        private static object SelectChannelReceiverPropString(DigitalAdsDb.CampaignProfile item)
        {
            if (item == null)
                return null;
            return $"Coin: {item.Coin}, Country: {item.Country},MaxAge: {item.maxAge},MinAge:{item.minAge}";
        }

        /// <summary>
        /// Get de todas as campanhas na BD
        /// </summary>
        /// <returns>Lista de campanhas</returns>
        [HttpGet]
        [Route("campanha")]
        public IHttpActionResult Get()
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.Campaign.ToList().Select(SelectCampaign).ToList();
                return Ok(item);
            }
        }

        /// <summary>
        /// Get de campanha expecifica por id
        /// </summary>
        /// <param name="id">id da campanha</param>
        /// <returns>Campanha</returns>
        [HttpGet]
        [Route("campanha/{id}")]
        public IHttpActionResult Get(int id)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.Campaign.FirstOrDefault(x => x.CampaignId == id);

                return Ok(SelectCampaign(item));
            }
        }

        /// <summary>
        /// Get the next hour available to send the campaign throw the channels taking in account 
        /// the countries that campaign should be send.
        /// </summary>
        /// <param name="id">id da campanha</param>
        /// <returns>Hour of the campaign</returns>
        [HttpGet]
        [Route("campanha/{id}/GetNextHourAvailabe")]
        public IHttpActionResult GetNextHourAvailabe(int id)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.Campaign.FirstOrDefault(x => x.CampaignId == id);
                return Ok(new {
                    hour = DateTime.UtcNow.AddHours(1).AddSeconds(15).ToString("yyyy-MM-ddTHH:mm:ss")
                });
            }
        }

        /// <summary>
        /// Get de conteudo de determinado canal para a campanha
        /// </summary>
        /// <param name="campanhaId">id da campanha</param>
        /// <param name="channelId">id do canal</param>
        /// <returns>Resultado do conteudo da campanha</returns>
        [HttpGet]
        [Route("campanha/{campanhaId}/conteudo/canal/{channel}/Tempo/{timePerMessage}")]
        public IHttpActionResult Get(int campanhaId, string channel, int timePerMessage)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.Campaign
                    .Where(x => x.CampaignId == campanhaId)
                    .ToList().Select(x => new
                    {
                        
                        Channel = x.CampaignChannel.Select(ch=> new MessageChannelQueueModel
                            {
                                Id = ch.IdChannel,
                                Name = ch.Channel.Name,
                                Body = ch.ChannelContent.Body,
                                LinkTemplate = ch.ChannelContent.LinkTemplate
                        }),
                        Campaign = x.Name,
                        x.CampaignProfile?.StartDate,
                        x.CampaignProfile?.ElapsedTime,
                        x.CampaignProfile?.maxAge,
                        x.CampaignProfile?.minAge,
                        Countries = x.CampaignProfile.Country.Split(';'),
                        x.CampaignProfile.Coin
                    }).FirstOrDefault();

                if (item == null) return BadRequest("Indisponivel");

               
                var maxRegistrys = Math.Round((double)(item.ElapsedTime.Value / timePerMessage ), MidpointRounding.AwayFromZero);
                var destinatarios = ctx.Destiny
                        .WhereIf(item.minAge.HasValue && item.maxAge.HasValue, x => x.Age >= item.minAge && x.Age <= item.maxAge)
                        .WhereIf(item.Countries.Any(), x => item.Countries.Contains(x.Country))
                        .Take(Convert.ToInt32(maxRegistrys)).ToList().Select(x => new MessageDestinyQueueModel
                        {
                            Id = x.DestinyId,
                            Name = x.Name,
                            Email = x.Email
                        }
                    );

                var queueModel = new MessageQueueModel
                {
                    Channel = item.Channel.ToList(),
                    Campanha = item.Campaign,
                    Destiny = destinatarios.ToList()

                };


                //var sendDate = item.StartDate;
                //var endDate = item.EndDate;
                //var newDate = sendDate.Value;
                //var firstClock = destinatarios.FirstOrDefault()?.FusoHorario;
                //foreach (var dest in destinatarios)
                //{
                //    if (firstClock != dest.FusoHorario)
                //    {
                //        sendDate = item.StartDate.GetValueOrDefault(DateTime.Now).AddHours(dest.FusoHorario);
                //        endDate = item.EndDate.GetValueOrDefault(DateTime.Now).AddHours(dest.FusoHorario);
                //        firstClock = dest.FusoHorario;
                //        newDate = sendDate.Value;
                //    }
                //    //TODO change this from minutes to seconds
                //    newDate = newDate.AddMinutes(5);
                //    //newDate = newDate.AddSeconds(5);
                //    if (newDate <= endDate)
                //    {
                //        queueModel.Destiny.Add(new MessageDestinyQueueModel
                //        {
                //            Name = dest.Name,
                //            Email = dest.Email,
                //            Id = dest.DestinyId,
                //            SendMessageDateTime = newDate
                //        });
                //    }

                //}

                return Ok(queueModel);
            }
        }

        /// <summary>
        /// Get de conteudo de determinado canal para a campanha
        /// </summary>
        /// <param name="campanhaId">id da campanha</param>
        /// <param name="channelId">id do canal</param>
        /// <returns>Resultado do conteudo da campanha</returns>
        [HttpGet]
        [Route("campanha/{campanhaId}/canal/{canal}/country/{country}/Tempo/{timePerMessage}")]
        public IHttpActionResult GetList(int campanhaId, string canal, string country, int timePerMessage)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.CampaignChannel
                    .Where(x => x.IdCampaign == campanhaId)
                    .Where(x => x.Channel.Name == canal)
                    .ToList().Select(x => new
                    {
                        x.ChannelContent.Body,
                        x.Campaign.CampaignProfile?.StartDate,
                        x.Campaign.CampaignProfile?.ElapsedTime,
                        x.Campaign.CampaignProfile?.maxAge,
                        x.Campaign.CampaignProfile?.minAge,
                        Countries = x.Campaign.CampaignProfile.Country.Split(';'),
                        x.Campaign.CampaignProfile.Coin,
                        x.ChannelContent.LinkTemplate

                    }).FirstOrDefault();

                if (item == null) return BadRequest("Indisponivel");


                var maxRegistrys = Math.Round((double)(item.ElapsedTime.Value / timePerMessage), MidpointRounding.AwayFromZero);
                var destinatarios = ctx.Destiny
                        .WhereIf(item.minAge.HasValue && item.maxAge.HasValue, x => x.Age >= item.minAge && x.Age <= item.maxAge)
                        .WhereIf(item.Countries.Any(), x => x.Country.Contains(country))
                        .Take(Convert.ToInt32(maxRegistrys)).ToList().Select(x => new MessageDestinyQueueModel
                        {
                            Id = x.DestinyId,
                            Name = x.Name,
                            Email = x.Email,
                            OutraPropriedade = x.Field??"",
                            Mensagem= item.Body.Replace("{LINK}", item.LinkTemplate)
                        }
                    );

                return Ok(new {destinatarios});
            }
        }

        private static int GetTimeZone(string country)
        {
            switch ((country??string.Empty).ToLower())
            {
                //case "pt": return 0;
                case "fr": return 1;
                case "es": return 1;
                case "ch": return 7;
                default: return 0;
            }
        }

        [HttpGet]
        [Route("campanha/{campanhaId}/canal/{channel}")]
        public IHttpActionResult GetCanalInfo(int campanhaId, string channel)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.CampaignChannel
                    .Where(x => x.Channel.Name == channel && x.IdCampaign == campanhaId)
                    .ToList().Select(SelectCampaignChannel).FirstOrDefault();



                return Ok(item);
            }
        }

        /// <summary>
        /// Cria uma nova campanha na BD
        /// </summary>
        /// <param name="item">Campanha</param>
        /// <returns>Resultado da criacao da campanha</returns>
        [HttpPost]
        [Route("campanha")]
        public IHttpActionResult Post([FromBody]DigitalAdsDb.Campaign item)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                //TODO create estado Criado
                // var item = value.DeserializeFromJson<Campaign>();
                if (item != null)
                {
                    var channels = ctx.Channel.ToList();

                    foreach (var channel in channels)
                    {
                        item.StateId = 1;
                        item.CampaignChannel.Add(new CampaignChannel
                        {
                            IdChannel = channel.ChannelId,


                        });
                    }
                    item.CampaignProfile = new CampaignProfile();
                    ctx.Campaign.Add(item);
                }
                ctx.SaveChanges();


                return Ok(SelectCampaign(item));
            }
        }

        /// <summary>
        /// Altera uma campanha existente
        /// </summary>
        /// <param name="id">id da campanha</param>
        /// <param name="newItem">informacao a alterar da campanha</param>
        /// <returns>Resultado da alteração a campanha</returns>
        [HttpPut]
        [Route("campanha/{id}")]
        public IHttpActionResult Put(int id, [FromBody]ProfileCampaingModel newItem)
        {
            /*
             * 
             {	
                "Coin": "EUR",
                "Country": "PT;ES;CH",
                "MaxAge": 45,
                "MinAge": 65,
                "EndDate": "30",
                "StartDate" : "15-05-2018 10:00"
             }
             */

            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.Campaign
                    .Single(x => x.CampaignId == id);


                if (newItem != null && item != null)
                {
                    DateTime? dateParsed = null;
                    try
                    {
                        dateParsed = newItem.StartDate != null ? Convert.ToDateTime(newItem.StartDate) : new DateTime(1900, 1, 1);
                    }
                    catch (Exception e)
                    {
                        dateParsed = new DateTime(1900, 1, 1);
                    }

                    item.Goal = newItem.Goal ?? item.Goal;
                    //item.LinkTemplate = newItem.LinkTemplate ?? item.LinkTemplate;
                    item.Name = newItem.Name ?? item.Name;
                    item.StateId = newItem.StateId ?? item.StateId;
                    item.NumberDestaniesSent = newItem.NumberDestaniesSent ?? item.NumberDestaniesSent;
                    item.NumberDestaniesFailed = newItem.NumberDestaniesFailed ?? item.NumberDestaniesFailed;


                    if (item.CampaignProfile != null)
                    {
                        item.CampaignProfile.Coin = newItem.Coin ?? item.CampaignProfile.Coin;
                        item.CampaignProfile.Country = newItem.Country ?? item.CampaignProfile.Country;
                        item.CampaignProfile.maxAge = newItem.MaxAge ?? item.CampaignProfile.maxAge;
                        item.CampaignProfile.minAge = newItem.MinAge ?? item.CampaignProfile.minAge;
                        item.CampaignProfile.ElapsedTime = newItem.ElapsedTime ?? item.CampaignProfile.ElapsedTime;
                        item.CampaignProfile.StartDate = dateParsed ?? item.CampaignProfile.StartDate;
                    }
                    else
                    {
                        item.CampaignProfile = new CampaignProfile
                        {
                            Coin = newItem.Coin,
                            Country = newItem.Country,
                            maxAge = newItem.MaxAge,
                            minAge = newItem.MinAge,
                            ElapsedTime = newItem.ElapsedTime.GetValueOrDefault(0),
                            StartDate = dateParsed
                        };
                    }
                    
                }

                ctx.SaveChanges();

                return Ok(SelectCampaign(item));
            }
        }

        /// <summary>
        /// Delete de campanha
        /// </summary>
        /// <param name="id">id da campanha a eliminar</param>
        [HttpDelete]
        [Route("campanha/{id}")]
        public void Delete(int id)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.Campaign
                    .Single(x => x.CampaignId == id);

                ctx.Campaign.Remove(item);
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Get de um report(informativo) relativo a uma campanha
        /// </summary>
        /// <param name="id">id da campanha</param>
        /// <returns>Informativo sobre campanha</returns>
        [HttpGet]
        [Route("campanha/{id}/report")]
        public IHttpActionResult GetReport(int id)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                try
                {
                    var item = ctx.Campaign
                        .Where(x => x.CampaignId == id).ToList()
                        .Select(SelectCampaignReport).FirstOrDefault();

                    return Ok(item);
                }
                catch (Exception e)
                {
                    return InternalServerError(e);
                }


            }
        }

        /// <summary>
        /// Get do proximo elemento a configurar numa campanha.
        /// 
        /// OBS: Mode = 0 significa que procura um canal com o body indefinido; 
        /// Mode != 0, procura um canal com as informaçoes associadas a campanha daquele canal, por criar
        /// </summary>
        /// <param name="campanhaId">id da campanha</param>
        /// <param name="mode">modo de operacao</param>
        /// <returns>Proximo canal por configurar encontrado</returns>
        [HttpGet]
        [Route("campanha/{campanhaId}/GetNextChannel/{mode}")]
        public IHttpActionResult GetNext(int campanhaId, int mode)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                switch (mode)
                {
                    case 0:
                        {
                            var channel = ctx.Channel
                                .Where(x =>
                                    x.CampaignChannel.Any(y =>
                                        y.IdCampaign == campanhaId && y.IdChannelContent == null) ||
                                    x.CampaignChannel.All(y => y.IdCampaign != campanhaId))
                                    .OrderBy(x => x.ChannelId).ToList().Select(x => new
                                    {
                                        x.ChannelId,
                                        x.Name
                                    }).FirstOrDefault();
                            if (channel != null)
                                return Ok(channel);


                        }
                        break;
                    default:
                        {
                            var channel = ctx.Channel
                                .Where(x =>
                                    x.CampaignChannel.Any(y =>
                                        y.IdCampaign == campanhaId && y.Campaign.CampaignProfile == null) ||
                                    x.CampaignChannel.All(y => y.IdCampaign != campanhaId))
                                .OrderBy(x => x.ChannelId).ToList().Select(x => new
                                {
                                    x.ChannelId,
                                    x.Name
                                }).FirstOrDefault();
                            if (channel != null)
                                return Ok(channel);

                        }
                        break;

                }


                return Ok(new Channel
                {
                    ChannelId = -1,
                    Name = ""
                });


            }
        }

        /// <summary>
        /// Get do proximo elemento por configurar numa campanha.
        /// </summary>
        /// <param name="campanhaId">ida da campanha</param>
        /// <returns>Proximo canal por configurar encontrado</returns>
        [HttpGet]
        [Route("campanha/{campanhaId}/GetNextChannel")]
        public IHttpActionResult GetNextChannelWithoutMode(int campanhaId)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                try
                {
                    var channel = ctx.Channel
                        .Where(x =>
                            x.CampaignChannel.Any(y =>
                                y.IdCampaign == campanhaId && y.IdChannelContent == null) ||
                            x.CampaignChannel.All(y => y.IdCampaign != campanhaId))
                        .OrderBy(x => x.ChannelId).ToList().Select(x => new
                        {
                            x.ChannelId,
                            x.Name
                        }).FirstOrDefault();

                    if (channel != null)
                    {
                        return Ok(channel);
                    }

                    channel = ctx.Channel
                        .Where(x =>
                            x.CampaignChannel.Any(y =>
                                y.IdCampaign == campanhaId && y.Campaign.CampaignProfile == null) ||
                            x.CampaignChannel.All(y => y.IdCampaign != campanhaId))
                        .OrderBy(x => x.ChannelId).ToList().Select(x => new
                        {
                            x.ChannelId,
                            x.Name
                        }).FirstOrDefault();

                    if (channel != null)
                        return Ok(channel);

                    return Ok(new Channel
                    {
                        ChannelId = -1,
                        Name = ""
                    });
                }
                catch (Exception e)
                {
                    return InternalServerError(e);
                }



            }
        }

        /// <summary>
        /// Altera o conteudo de uma associação de canal a campanha
        /// </summary>
        /// <param name="idCampanha">id da campanha</param>
        /// <param name="idChannel">id do canal</param>
        /// <param name="newItem">informaçoes a alterar</param>
        /// <returns>Informacoa do objecto actualizado</returns>
        [HttpPut]
        [Route("campanha/{idCampanha}/canal/{channel}")]
        public IHttpActionResult PutChannelContent(int idCampanha, string channel, [FromBody]ContentChannelModel newItem)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                try
                {
                    var item = ctx.CampaignChannel
                        .Single(x => x.Channel.Name == channel && x.IdCampaign == idCampanha);

                    if (item == null)
                        return BadRequest("Canal não existente.");

                    if (newItem != null && item != null)
                    {
                        if (newItem.Body != null || newItem.ChannelContent != null)
                        {
                            var content = newItem.Body ?? newItem.ChannelContent;
                            if (item.ChannelContent == null)
                            {
                                item.ChannelContent = new ChannelContent
                                {
                                    Body = content
                                };
                            }
                            else
                            {
                                item.ChannelContent.Body = newItem.Body ?? item.ChannelContent.Body;
                            }

                        }

                    }
                    ctx.SaveChanges();

                    return Ok(SelectCampaignChannel(item));
                }
                catch (Exception e)
                {
                    return InternalServerError(e);
                }

            }
        }

        [HttpPut]

        [Route("campanha/{idCampanha}/status")]
        public IHttpActionResult SetCampaignStatus(int idCampanha, [FromBody] StatusCampaignModel statusModel)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                try
                {
                    if (statusModel == null) return BadRequest("Model não definido");

                    var statusDb = ctx.State.FirstOrDefault(x => x.Name == statusModel.Status);
                    if (statusDb == null)
                    {
                        return BadRequest("Não existe o status.");
                    }
                    var statusId = statusDb.StateId;


                    var item = ctx.Campaign
                        .Single(x => x.CampaignId == idCampanha);


                    if (item != null)
                    {
                        item.StateId = statusId;
                        item.ReviewComment = statusModel.Comment ?? item.ReviewComment;
                    }
                    ctx.SaveChanges();

                    return Ok(SelectCampaign(item));
                }
                catch (Exception e)
                {
                    return InternalServerError(e);
                }

            }
        }

        //TODO criar get estados

        //[HttpPost]
        //[Route("campanha/{CampanhaId}/canal/{ChannelId}")]
        //public IHttpActionResult SetChannelToCampaign([FromBody]DigitalAdsDb.CampaignChannel item)
        //{
        //    //todo set body coin country
        //    using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
        //    {
        //        try
        //        {
        //            ctx.CampaignChannel.Add(item);
        //            ctx.SaveChanges();

        //            return Ok(item.ChannelContent);
        //        }
        //        catch (Exception e)
        //        {
        //            return InternalServerError(e);
        //        }

        //    }
        //}

    }
}
