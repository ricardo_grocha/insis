﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalAdsDb;
using DigitalAdsDb.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CampaignApi.Controllers
{
    [Route("api/[controller]")]
    public class CampanhaController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public string Get()
        {
            using (var ctx = new DigitalAdsDb.Models.DigitalAdsContext())
            {
                var item = ctx.Campaign.ToList();

                return item.Serialize2Json();
            }
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            using (var ctx = new DigitalAdsDb.Models.DigitalAdsContext())
            {
                var item = ctx.Campaign
                    .Where(x=>x.Id==id)
                    .ToList();

                return item.Serialize2Json();
            }
        }

        // GET api/<controller>/5
        [HttpGet("{campanhaId}/Conteudo/Canal/{channelId}")]
        public string Get(int campanhaId, int channelId)
        {
            using (var ctx = new DigitalAdsDb.Models.DigitalAdsContext())
            {
                var item = ctx.CampaignHasChannels
                    .Where(x=>x.IdChannel==channelId && x.IdCampaign==campanhaId)
                    .Select(x=> new {Campanha = x.IdCampaignNavigation, Canal=x.IdChannelNavigation})
                    .ToList();

                return item.Serialize2Json();
            }
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]Campaign item)
        {
            using (var ctx = new DigitalAdsDb.Models.DigitalAdsContext())
            {
               // var item = value.DeserializeFromJson<Campaign>();
                if (item != null)
                {
                    ctx.Campaign.Add(item);
                }
                ctx.SaveChanges();
            }
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Campaign newItem)
        {
            using (var ctx = new DigitalAdsDb.Models.DigitalAdsContext())
            {
                var item = ctx.Campaign
                    .Single(x => x.Id == id);

   
                if (newItem != null && item != null)
                {
                    item.Goal = newItem.Goal;
                    item.Name = newItem.Name;
                    item.StateId = newItem.StateId;
                }
                ctx.SaveChanges();
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            using (var ctx = new DigitalAdsDb.Models.DigitalAdsContext())
            {
                var item = ctx.Campaign
                    .Single(x => x.Id == id);

                ctx.Campaign.Remove(item);
                ctx.SaveChanges();
            }
        }
    }
}

