﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CampaignApi.Models
{
    public class MessageQueueModel
    {
        public string Content { get; set; }
        public string Campanha { get; set; }
        public List<MessageChannelQueueModel> Channel { get; set; } = new List<MessageChannelQueueModel>();
        public List<MessageDestinyQueueModel> Destiny { get; set; } = new List<MessageDestinyQueueModel>();

    }

    public class MessageDestinyQueueModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Mensagem { get; set; }
        public string Email { get; set; }
        public string OutraPropriedade { get; set; }
        //public DateTime SendMessageDateTime { get; set; }

    }

    public class MessageChannelQueueModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Body { get; set; }
        public string LinkTemplate { get; set; }
        //public DateTime SendMessageDateTime { get; set; }

    }
}