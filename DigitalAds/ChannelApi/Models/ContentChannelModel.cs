using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CampaignApi.Models
{
    public class ContentChannelModel
    {
        public string Body { get; set; }
        public string ChannelContent { get; set; }
        public string LinkTemplate { get; set; }

    }
    public class ProfileCampaingModel
    {
        public string Goal { get; set; }
        public string Name { get; set; }

        public int? StateId { get; set; }

        public string Coin { get; set; }
        public string Country { get; set; }
        public int? MaxAge { get; set; }
        public int? MinAge { get; set; }
        public string StartDate { get; set; }
        public int? ElapsedTime { get; set; }
        public int? NumberDestaniesSent { get; set; }
        public int? NumberDestaniesFailed { get; set; }
    }

    public class UrlModel
    {
        public string Id { get; set; }
        public int CampaignID { get; set; } //
        public string ChannelID { get; set; } //
        public string RecipientEmail { get; set; } //
        public bool HadAccess { get; set; }
        public string LinkTemplate { get; set; } //
        public string Link { get; set; }
    }
}