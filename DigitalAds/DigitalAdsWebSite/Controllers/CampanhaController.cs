﻿using DigitalAdsWebSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace DigitalAdsWebSite.Controllers
{
    public class CampanhaController : Controller
    {
        // GET: User
        public async System.Threading.Tasks.Task<ActionResult> index(string hash)
        {
            //connect to web service to validate hash
            using (var client = new HttpClient())
            {
                var baseUri = new Uri("http://localhost:5100/api/URLManager/" + hash);

                await client.PutAsync(baseUri + "/Access", null);
                var response = await client.GetAsync(baseUri);

                string json = await response.Content.ReadAsStringAsync();
                ViewBag.Campaign = Newtonsoft.Json.JsonConvert.DeserializeObject<Campaign>(json);

                if (true)
                {
                    ViewBag.Message = "A sua subscricao ira ser efectivada nos proximos dias.";
                    //Send to ESB
                }
                else
                {
                    ViewBag.Message = "Link invalido ou ja se incontra activo.";
                }
            }
            
            return View();
        }

    }
}