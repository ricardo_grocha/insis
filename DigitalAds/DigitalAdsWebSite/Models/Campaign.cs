﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigitalAdsWebSite.Models
{
    public class Campaign
    {
        public string Id { get; set; }
        public int CampaignID { get; set; }
        public string ChannelID { get; set; }
        public string RecipientEmail { get; set; }
        public bool HadAccess { get; set; }
        public string LinkTemplate { get; set; }
        public string Link { get; set; }
    }
}