﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalAdsDb;
using DigitalAdsDb.Models;
using Microsoft.AspNetCore.Mvc;

namespace DestinyApi.Controllers
{
    [Route("api/[controller]")]
    public class DestinatarioController : Controller
    {
        // GET: api/<controller>
        [HttpGet]
        public string Get()
        {
            using (var ctx = new DigitalAdsDb.Models.DigitalAdsContext())
            {
                var item = ctx.Destiny.ToList();

                return item.Serialize2Json();
            }
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            using (var ctx = new DigitalAdsDb.Models.DigitalAdsContext())
            {
                var item = ctx.Destiny
                    .Where(x => x.Id == id)
                    .ToList();

                return item.Serialize2Json();
            }
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]Destiny item)
        {
            using (var ctx = new DigitalAdsDb.Models.DigitalAdsContext())
            {
                if (item != null)
                {
                    ctx.Destiny.Add(item);
                }
                ctx.SaveChanges();
            }
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Destiny newItem)
        {
            using (var ctx = new DigitalAdsDb.Models.DigitalAdsContext())
            {
                var item = ctx.Destiny
                    .Single(x => x.Id == id);

               if (newItem != null && item != null)
                {
                    item.Name = newItem.Name;
                }
                ctx.SaveChanges();
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            using (var ctx = new DigitalAdsDb.Models.DigitalAdsContext())
            {
                var item = ctx.Campaign
                    .Single(x => x.Id == id);

                ctx.Campaign.Remove(item);
                ctx.SaveChanges();
            }
        }
    }
}
