﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using DigitalAdsDatabase;
using DigitalAdsDb;


namespace ChannelApi.Controllers
{
    public class ChannelController : ApiController
    {

        /// <summary>
        /// Select de infomação de canal
        /// </summary>
        /// <param name="item">Canal</param>
        /// <returns></returns>
        private static object SelectChannel(DigitalAdsDb.Channel item)
        {
            return new
            {
                item.ChannelId,
                item.Name,
            };
        }

        /// <summary>
        /// Get de todos os canais da BD
        /// </summary>
        /// <returns>Lista de canais</returns>
        [HttpGet]
        [Route("canal")]
        public IHttpActionResult Get()
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.Channel.ToList().Select(SelectChannel);

                return Ok(item);
            }
        }

        /// <summary>
        /// Get de informacoes de um canal especifico
        /// </summary>
        /// <param name="id">id do canal</param>
        /// <returns>Canal selecionado</returns>
        [Route("canal/{id}")]
        public IHttpActionResult Get(int id)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.Channel
                    .Where(x => x.ChannelId == id)
                    .ToList().Select(SelectChannel);

                return Ok(item);
            }
        }
        /// <summary>
        /// Criação de novo canal
        /// </summary>
        /// <param name="item">Canal</param>
        /// <returns>Item criado</returns>
        // POST api/<controller>
        [Route("canal")]

        public IHttpActionResult Post([FromBody]Channel item)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                if (item != null)
                {
                    ctx.Channel.Add(item);
                }
                ctx.SaveChanges();
                return Ok(item);
            }
        }

        /// <summary>
        /// Alteracao de um canal
        /// </summary>
        /// <param name="id">id do canal</param>
        /// <param name="newItem">informacao a alterar a um canal</param>
        /// <returns>canal apos alteracao</returns>
        // PUT api/<controller>/5
        [Route("canal/{id}")]
        public IHttpActionResult Put(int id, [FromBody]Channel newItem)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.Channel
                    .Single(x => x.ChannelId == id);

                if (newItem != null && item != null)
                {
                    item.Name = newItem.Name;
                }
                ctx.SaveChanges();
                return Ok(item);
            }
        }

        /// <summary>
        /// Delete de um canal na BD
        /// </summary>
        /// <param name="id">Id do canal a remover</param>
        // DELETE api/<controller>/5
        [Route("canal/{id}")]
        public void Delete(int id)
        {
            using (var ctx = new DigitalAdsDb.DigitalAdsEntities())
            {
                var item = ctx.Channel
                    .Single(x => x.ChannelId == id);

                ctx.Channel.Remove(item);
                ctx.SaveChanges();
            }
        }
    }
}
