/****** Object:  User [digitalroot]    Script Date: 13/03/2018 09:48:58 ******/
CREATE USER [digitalroot] FOR LOGIN [digitalroot] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[Campaign]    Script Date: 13/03/2018 09:48:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Campaign](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Goal] [varchar](max) NULL,
	[StateId] [int] NULL,
 CONSTRAINT [PK_Campaign] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CampaignHasChannels]    Script Date: 13/03/2018 09:49:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CampaignHasChannels](
	[IdCampaign] [int] NOT NULL,
	[IdChannel] [int] NOT NULL,
 CONSTRAINT [PK_CampaignHasChannels] PRIMARY KEY CLUSTERED 
(
	[IdCampaign] ASC,
	[IdChannel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Channel]    Script Date: 13/03/2018 09:49:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Channel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Channel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChannelHasDestiny]    Script Date: 13/03/2018 09:49:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChannelHasDestiny](
	[IdChannel] [int] NOT NULL,
	[IdDestiny] [int] NOT NULL,
 CONSTRAINT [PK_ChannelHasDestiny] PRIMARY KEY CLUSTERED 
(
	[IdChannel] ASC,
	[IdDestiny] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Destiny]    Script Date: 13/03/2018 09:49:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Destiny](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Destiny] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[State]    Script Date: 13/03/2018 09:49:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[State](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Campaign] ON 

INSERT [dbo].[Campaign] ([Id], [Name], [Goal], [StateId]) VALUES (3, N'Teste', N'Teste', 1)
INSERT [dbo].[Campaign] ([Id], [Name], [Goal], [StateId]) VALUES (4, N'Teste2', N'Teste2', 1)
SET IDENTITY_INSERT [dbo].[Campaign] OFF
INSERT [dbo].[CampaignHasChannels] ([IdCampaign], [IdChannel]) VALUES (3, 1)
INSERT [dbo].[CampaignHasChannels] ([IdCampaign], [IdChannel]) VALUES (3, 2)
INSERT [dbo].[CampaignHasChannels] ([IdCampaign], [IdChannel]) VALUES (4, 1)
SET IDENTITY_INSERT [dbo].[Channel] ON 

INSERT [dbo].[Channel] ([Id], [Name]) VALUES (1, N'Gmail')
INSERT [dbo].[Channel] ([Id], [Name]) VALUES (2, N'Facebook')
SET IDENTITY_INSERT [dbo].[Channel] OFF
SET IDENTITY_INSERT [dbo].[State] ON 

INSERT [dbo].[State] ([Id], [Name]) VALUES (1, N'Created')
INSERT [dbo].[State] ([Id], [Name]) VALUES (2, N'Submited')
INSERT [dbo].[State] ([Id], [Name]) VALUES (3, N'Canceled')
SET IDENTITY_INSERT [dbo].[State] OFF
ALTER TABLE [dbo].[Campaign]  WITH CHECK ADD  CONSTRAINT [FK_Campaign_State] FOREIGN KEY([StateId])
REFERENCES [dbo].[State] ([Id])
GO
ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK_Campaign_State]
GO
ALTER TABLE [dbo].[CampaignHasChannels]  WITH CHECK ADD  CONSTRAINT [FK_CampaignHasChannels_Campaign] FOREIGN KEY([IdCampaign])
REFERENCES [dbo].[Campaign] ([Id])
GO
ALTER TABLE [dbo].[CampaignHasChannels] CHECK CONSTRAINT [FK_CampaignHasChannels_Campaign]
GO
ALTER TABLE [dbo].[CampaignHasChannels]  WITH CHECK ADD  CONSTRAINT [FK_CampaignHasChannels_Channel] FOREIGN KEY([IdChannel])
REFERENCES [dbo].[Channel] ([Id])
GO
ALTER TABLE [dbo].[CampaignHasChannels] CHECK CONSTRAINT [FK_CampaignHasChannels_Channel]
GO
ALTER TABLE [dbo].[ChannelHasDestiny]  WITH CHECK ADD  CONSTRAINT [FK_ChannelHasDestiny_Channel] FOREIGN KEY([IdChannel])
REFERENCES [dbo].[Channel] ([Id])
GO
ALTER TABLE [dbo].[ChannelHasDestiny] CHECK CONSTRAINT [FK_ChannelHasDestiny_Channel]
GO
ALTER TABLE [dbo].[ChannelHasDestiny]  WITH CHECK ADD  CONSTRAINT [FK_ChannelHasDestiny_Destiny] FOREIGN KEY([IdDestiny])
REFERENCES [dbo].[Destiny] ([Id])
GO
ALTER TABLE [dbo].[ChannelHasDestiny] CHECK CONSTRAINT [FK_ChannelHasDestiny_Destiny]
GO