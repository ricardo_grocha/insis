﻿using System;
using System.Collections.Generic;

namespace DigitalAdsDb.Models
{
    public partial class Channel
    {
        public Channel()
        {
            CampaignHasChannels = new HashSet<CampaignHasChannels>();
            ChannelHasDestiny = new HashSet<ChannelHasDestiny>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<CampaignHasChannels> CampaignHasChannels { get; set; }
        public ICollection<ChannelHasDestiny> ChannelHasDestiny { get; set; }
    }
}
