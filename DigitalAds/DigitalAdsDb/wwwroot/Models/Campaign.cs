﻿using System;
using System.Collections.Generic;

namespace DigitalAdsDb.Models
{
    public partial class Campaign
    {
        public Campaign()
        {
            CampaignHasChannels = new HashSet<CampaignHasChannels>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Goal { get; set; }
        public int? StateId { get; set; }

        public State State { get; set; }
        public ICollection<CampaignHasChannels> CampaignHasChannels { get; set; }
    }
}
