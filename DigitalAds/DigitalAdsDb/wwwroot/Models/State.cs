﻿using System;
using System.Collections.Generic;

namespace DigitalAdsDb.Models
{
    public partial class State
    {
        public State()
        {
            Campaign = new HashSet<Campaign>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Campaign> Campaign { get; set; }
    }
}
