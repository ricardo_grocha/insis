﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DigitalAdsDb.Models
{
    public partial class DigitalAdsContext : DbContext
    {
        public virtual DbSet<Campaign> Campaign { get; set; }
        public virtual DbSet<CampaignHasChannels> CampaignHasChannels { get; set; }
        public virtual DbSet<Channel> Channel { get; set; }
        public virtual DbSet<ChannelHasDestiny> ChannelHasDestiny { get; set; }
        public virtual DbSet<Destiny> Destiny { get; set; }
        public virtual DbSet<State> State { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Data Source=LAPTOP-JAOTUREN\GAMSQLSERVER;Initial Catalog=DigitalAds;Persist Security Info=True;User ID=sa;Password=admingam;TrustServerCertificate=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Campaign>(entity =>
            {
                entity.Property(e => e.Goal).IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.State)
                    .WithMany(p => p.Campaign)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_Campaign_State");
            });

            modelBuilder.Entity<CampaignHasChannels>(entity =>
            {
                entity.HasKey(e => new { e.IdCampaign, e.IdChannel });

                entity.HasOne(d => d.IdCampaignNavigation)
                    .WithMany(p => p.CampaignHasChannels)
                    .HasForeignKey(d => d.IdCampaign)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CampaignHasChannels_Campaign");

                entity.HasOne(d => d.IdChannelNavigation)
                    .WithMany(p => p.CampaignHasChannels)
                    .HasForeignKey(d => d.IdChannel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CampaignHasChannels_Channel");
            });

            modelBuilder.Entity<Channel>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ChannelHasDestiny>(entity =>
            {
                entity.HasKey(e => new { e.IdChannel, e.IdDestiny });

                entity.HasOne(d => d.IdChannelNavigation)
                    .WithMany(p => p.ChannelHasDestiny)
                    .HasForeignKey(d => d.IdChannel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ChannelHasDestiny_Channel");

                entity.HasOne(d => d.IdDestinyNavigation)
                    .WithMany(p => p.ChannelHasDestiny)
                    .HasForeignKey(d => d.IdDestiny)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ChannelHasDestiny_Destiny");
            });

            modelBuilder.Entity<Destiny>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<State>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
        }
    }
}
