﻿using System;
using System.Collections.Generic;

namespace DigitalAdsDb.Models
{
    public partial class Destiny
    {
        public Destiny()
        {
            ChannelHasDestiny = new HashSet<ChannelHasDestiny>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<ChannelHasDestiny> ChannelHasDestiny { get; set; }
    }
}
