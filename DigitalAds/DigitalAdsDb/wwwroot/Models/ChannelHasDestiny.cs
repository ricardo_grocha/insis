﻿using System;
using System.Collections.Generic;

namespace DigitalAdsDb.Models
{
    public partial class ChannelHasDestiny
    {
        public int IdChannel { get; set; }
        public int IdDestiny { get; set; }

        public Channel IdChannelNavigation { get; set; }
        public Destiny IdDestinyNavigation { get; set; }
    }
}
