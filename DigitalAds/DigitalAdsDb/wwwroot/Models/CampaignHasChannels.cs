﻿using System;
using System.Collections.Generic;

namespace DigitalAdsDb.Models
{
    public partial class CampaignHasChannels
    {
        public int IdCampaign { get; set; }
        public int IdChannel { get; set; }

        public Campaign IdCampaignNavigation { get; set; }
        public Channel IdChannelNavigation { get; set; }
    }
}
