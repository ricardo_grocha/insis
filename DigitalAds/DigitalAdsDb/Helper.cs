﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Xml;
using Newtonsoft.Json;
using Formatting = Newtonsoft.Json.Formatting;

namespace DigitalAdsDatabase
{
    public static class DatabaseHelper
    {
        /**
 * How To: https://docs.microsoft.com/en-us/ef/core/get-started/aspnetcore/existing-db
 * 
 * Install-Package Microsoft.EntityFrameworkCore.SqlServer
 * Install-Package Microsoft.EntityFrameworkCore.Tools
 * Install-Package Microsoft.VisualStudio.Web.CodeGeneration.Design
 * 
 * Scaffold-DbContext  "Data Source=RETORTA\SQLEXPRESS;Initial Catalog=DigitalAds;Persist Security Info=True;User ID=digitalroot;Password=!aaaAAA123!;TrustServerCertificate=True" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models
 * 
 */

        public static string StringAggregation(this IEnumerable<string> source, Func<string, string, string> predicate, string defaultValue = "")
        {
            if (source != null && source.Any())
            {
                return source.Aggregate(predicate);
            }
            return defaultValue;
        }

        public static string Serialize2Json<T>(this T lista)
        {
            //return  JsonConvert.SerializeObject(lista);

            var json = JsonConvert.SerializeObject(lista, typeof(T), Formatting.None, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.None
            });

            return json;
        }

        public static T DeserializeFromJson<T>(this string objecto)
        {
            return JsonConvert.DeserializeObject<T>(objecto, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                TypeNameHandling = TypeNameHandling.All
            });

        }

        /// <summary>
        /// Provided dynamic queyring based on condition result.
        /// </summary>
        public static IQueryable<TSource> WhereIf<TSource>(this IQueryable<TSource> source, bool condition, Expression<Func<TSource, bool>> predicate)
        {
            return condition ? source.Where(predicate) : source;
        }


        /// <summary>
        /// Provided dynamic queyring based on condition result.
        /// </summary>
        public static IList<TSource> WhereIf<TSource>(this IList<TSource> source, bool condition, Expression<Func<TSource, bool>> predicate)
        {
            return source.AsQueryable().WhereIf(condition, predicate).ToList();
        }


        private static Random CreateRandom()
        {
            byte[] gb = Guid.NewGuid().ToByteArray();
            int seed = BitConverter.ToInt32(gb, 0);
            return new Random(seed);
        }

        public static int GenerateNumber(int first, int last)
        {
            Random rnd = CreateRandom();

            return rnd.Next(first, last);
        }


    }

}
