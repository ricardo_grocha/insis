//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DigitalAdsDb
{
    using System;
    using System.Collections.Generic;
    
    public partial class ChannelContent
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ChannelContent()
        {
            this.CampaignChannel = new HashSet<CampaignChannel>();
        }
    
        public int ChannelContentId { get; set; }
        public string Body { get; set; }
        public string LinkTemplate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CampaignChannel> CampaignChannel { get; set; }
    }
}
